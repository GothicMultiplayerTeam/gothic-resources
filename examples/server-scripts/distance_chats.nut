local chats = [];

//Examples
addEventHandler("onInit", function(){
  AddChat(["ja", "me"], 1500, "$message ($player)");
  AddChat("k", 1600, "$player krzyczy: $message", 255, 50, 50);
  AddChat("sz", 700, "$player szepcze: $message", 150, 150, 150);
  AddChat("b", 1500, "(OOC) $player: $message");
});

function AddChat(cmd, distance, format, r = 255, g = 255, b = 255){
  local id = chats.len();

  if(format.find("$player") == null) { print("Cannot find $player var"); return -1; }
  if(format.find("$message") == null) { print("Cannot find $message var"); return -1; }

  chats.push({id = id, cmds = cmd, distance = distance, format = format, color = [r, g, b]});
  print("Added new chat(" + id + ")");

  return id;
}

local _replace = function(str, word, toword){
  if(str.find(word) == null) return null;

  local startpos = str.find(word);
  local endpos = str.find(word) + word.len();

  str = str.slice(0, startpos) + toword + str.slice(endpos);
  return str;
}

local _sendDistance = function(pid, chat, msg){
  local str = chat.format;
  str = _replace(str, "$player", getPlayerName(pid));
  str = _replace(str, "$message", msg);

  local pos = getPlayerPosition(pid);
  for(local i = 0; i < getMaxSlots(); ++i){
    if(isPlayerConnected(i)){
      local to = getPlayerPosition(i);
      if(getDistance3d(pos.x, pos.y, pos.z, to.x, to.y, to.z) <= chat.distance){
        sendMessageToPlayer(i, chat.color[0], chat.color[1], chat.color[2], str);
      }
    }
  }
}

addEventHandler("onPlayerCommand", function(pid, cmd, param){
  cmd = cmd.tolower();
  for(local i = 0; i < chats.len(); ++i){
    local chat = chats[i];
    if(typeof chat.cmds == "array"){
      for(local j = 0; j < chat.cmds.len(); ++j){
        if(cmd == chat.cmds[j]){
          _sendDistance(pid, chat, param);
          return;
        }
      }
    } else {
      if(typeof chat.cmds == "string" && cmd == chat.cmds){
        _sendDistance(pid, chat, param);
        return;
      }
    }
  }
});
